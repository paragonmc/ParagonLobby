package fr.discowzombie.paragonlobby.commands;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SpawnCmd implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(sender instanceof Player){
			Player p = (Player) sender;
			Location spawn = new Location(Bukkit.getWorld("lobby"), 0.4714480661894018, 68.0, 0.5121354362420228, -180.14987f, 1.0499977f);
			p.teleport(spawn);
		}
		return true;
	}

}

package fr.discowzombie.paragonlobby;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import fr.discowzombie.paragonlobby.commands.SpawnCmd;
import fr.discowzombie.paragonlobby.events.EventListeners;

public class ParagonLobby extends JavaPlugin {
	

	@Override
	public void onEnable() {
		
		Bukkit.getServer().getPluginManager().registerEvents(new EventListeners(), this);
		
		getCommand("spawn").setExecutor(new SpawnCmd());

		super.onEnable();
	}
	
	@Override
	public void onDisable() {
		super.onDisable();
	}

}

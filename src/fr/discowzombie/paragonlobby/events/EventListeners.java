package fr.discowzombie.paragonlobby.events;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerToggleFlightEvent;

public class EventListeners implements Listener {
	
	@EventHandler (ignoreCancelled = true, priority = EventPriority.LOWEST)
	public void onJ(PlayerJoinEvent e){
		Player p = e.getPlayer();
		
		e.setJoinMessage("�7[�a+�7] "+p.getDisplayName()+" �7[�a+�7]");
		
		if(!p.hasPermission("paragon.build")){
			p.setGameMode(GameMode.ADVENTURE);
		}
		Location spawn = new Location(Bukkit.getWorld("lobby"), 0.4714480661894018, 68.0, 0.5121354362420228, -180.14987f, 1.0499977f);
		p.teleport(spawn);
	}
	
	@EventHandler (ignoreCancelled = true)
	public void onL(PlayerQuitEvent e){
		Player p = e.getPlayer();
		e.setQuitMessage("�7[�c-�7] "+p.getDisplayName()+" �7[�c-�7]");
	}
	
	@EventHandler
	public void onB(BlockBreakEvent e){
		Player p = e.getPlayer();
		if((!p.getGameMode().equals(GameMode.CREATIVE)) || (!p.hasPermission("paragon.build"))){
			e.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onB(BlockPlaceEvent e){
		Player p = e.getPlayer();
		if((!p.getGameMode().equals(GameMode.CREATIVE)) || (!p.hasPermission("paragon.build"))){
			e.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onDamage(EntityDamageEvent e){
		if(e.getEntity() instanceof Player){
			e.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onFood(FoodLevelChangeEvent e){
		e.setCancelled(true);
	}
	
	ArrayList<Player> inFly = new ArrayList<>();
	
	@EventHandler 
	public void onPlayerToggleFlight(PlayerToggleFlightEvent event) { 
		final Player player = event.getPlayer(); 
		if(player.getGameMode() == GameMode.CREATIVE){
			return; 
		}
		event.setCancelled(true); 
		player.setAllowFlight(false); 
		player.setFlying(false); 
		player.setVelocity(player.getLocation().getDirection().multiply(1.5).setY(1.2)); 
		inFly.add(player);
	} 
	
	@SuppressWarnings("deprecation")
	@EventHandler 
	public void onPlayerMove(PlayerMoveEvent event) { 
		Player player = event.getPlayer(); 
		if((player.getGameMode() != GameMode.CREATIVE) && (player.isOnGround())){
			player.setAllowFlight(true); 
		} 
		if(inFly.contains(player) && player.isOnGround()){
			player.playEffect(player.getLocation(), Effect.SMOKE, 0);
			inFly.remove(player);
		}
		for(Player e : inFly){
			e.playEffect(e.getLocation(), Effect.SMOKE, 0);
		}
	}

}
